#! /usr/bin/env python
# coding: utf-8

"""
OpenDesktopServer

OpenDesktopServer is a part of OpenDesktopSharing


The OpenDesktopSharing's goal is to access to remote computer across firewall and proxy:
- No needed to know IPs address
- Access only need KEY/PASSWORD
- Use websocket VPN to connect server and client
- Use Auth server (OpenDesktopServer) to generate auth access

@author: Pierre Arnaud <parnaud@easter-eggs.com>
@copyright: 2017

OpenDesktopServer is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

OpenDesktopServer is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with OpenDesktopServer.  If not, see <http://www.gnu.org/licenses/>.
"""

import hashlib
import logging
import logging.handlers
import os
import os.path
import random
import sys
from ConfigParser import SafeConfigParser
from optparse import OptionParser

from SimpleWebSocketServer import SimpleWebSocketServer as SimpleWebSocketServer_
from SimpleWebSocketServer import WebSocket as WebSocket_

log = logging.getLogger('OpenDesktopServer')


def logging_setup(options):

    l = logging.getLogger()
    if options.debug:
        l.setLevel(logging.DEBUG)
    else:
        l.setLevel(logging.INFO)

    # Console handler (root logger)
    handler = logging.StreamHandler(sys.stderr)
    if options.debug:
        handler.setLevel(logging.DEBUG)
    elif options.info:
        handler.setLevel(logging.INFO)
    else:
        handler.setLevel(logging.ERROR)

    formatter = logging.Formatter('%(asctime)s [%(name)s] %(levelname)-5.5s %(message)s')
    handler.setFormatter(formatter)

    l.addHandler(handler)


sessions = {}


class SimpleWebSocketServer(SimpleWebSocketServer_):
    def __init__(self, host, port, websocketclass, config, selectInterval=0.01):
        # calling inherited
        super(SimpleWebSocketServer, self).__init__(host, port, websocketclass, selectInterval=selectInterval)

        self.config = config

    def _constructWebSocket(self, sock, address):
        return self.websocketclass(self, sock, address, self.config)


class WebSocket(WebSocket_):
    def __init__(self, server, sock, address, config):
        self.config = config
        super(WebSocket, self).__init__(server, sock, address)


class OpenDesktopServer(WebSocket):

    def handleMessage(self):
        log.debug('RECEIVE: {}'.format(str(len(self.data))))
        if 'SERVICE - ' in self.data:
            log.debug(self.data)
            if 'SERVICE - ASK HELP' in self.data:
                channel, key, password, vnc_password = None, None, None, None
                generate_key_password = True
                while generate_key_password is not None:
                    key = str(random.randint(self.config.getint('key', 'min'), self.config.getint('key', 'max')))
                    password = str(random.randint(self.config.getint('password', 'min'), self.config.getint('password', 'max')))
                    vnc_password = str(random.randint(self.config.getint('vnc_password', 'min'), self.config.getint('vnc_password', 'max')))
                    channel = str(hashlib.sha512('{}-{}'.format(key, password)).hexdigest())
                    if sessions.get(channel) is None:
                        generate_key_password = None
                sessions[channel] = dict()
                sessions[channel]['CONNECTIONS'] = dict()
                sessions[channel]['CONNECTIONS']['ASK'] = self
                sessions[channel]['VNC_PASSWORD'] = vnc_password
                response = 'SERVICE - OK ASK: {} {} {} {}'.format(
                    key,
                    password,
                    vnc_password,
                    channel
                )
                sessions[channel]['CONNECTIONS']['ASK'].sendMessage(response)
            elif 'SERVICE - GIVE HELP: ' in self.data:
                key_password = '-'.join(self.data.split(': ')[1].split('-'))
                channel = str(hashlib.sha512(key_password).hexdigest())
                if sessions.get(channel):
                    if sessions[channel]['CONNECTIONS'].get('GIVE') is None:
                        sessions[channel]['CONNECTIONS']['GIVE'] = self
                        vnc_password = sessions[channel]['VNC_PASSWORD']
                        response = 'SERVICE - OK GIVE: {} {}'.format(
                            vnc_password,
                            channel
                        )
                        sessions[channel]['CONNECTIONS']['GIVE'].sendMessage(response)
                        sessions[channel]['CONNECTIONS']['ASK'].sendMessage('SERVICE - OK FORWARDING')
                    else:
                        self.sendMessage('SERVICE - SESSION EXPIRED')
                        log.debug('SERVICE - SESSION EXPIRED')
                else:
                    log.debug('SERVICE - SESSION NOT FOUND')
                    self.sendMessage('SERVICE - SESSION NOT FOUND')
            elif 'SERVICE - CONNECTED' in self.data:
                channel = self.data.split('SERVICE - CONNECTED ')[1]
                sessions[channel]['CONNECTIONS']['ASK'].sendMessage(self.data)
        else:
            channel = str(self.data[:128])
            if sessions.get(channel):
                for task, connexion in sessions[channel]['CONNECTIONS'].iteritems():
                    if connexion != self:
                        log.debug(
                            'FORWARDING: {} {} to ...{}'.format(
                                task,
                                str(len(self.data[128:])),
                                channel[:5]
                            )
                        )
                        connexion.sendMessage(self.data[128:])
                        break

    def handleClose(self):
        log.info("Closing: {} - {}".format(self.address[0], str(self.headerbuffer)))
        channels = set()
        for channel, session in sessions.iteritems():
            if sessions.get(channel):
                if self in [sessions[channel]['CONNECTIONS'].get('ASK'), sessions[channel]['CONNECTIONS'].get('GIVE')]:
                    if sessions[channel]['CONNECTIONS'].get('ASK'):
                        sessions[channel]['CONNECTIONS']['ASK'].sendMessage('SERVICE - CLOSE')
                        log.info('CLOSING: {}'.format(sessions[channel]['CONNECTIONS']['ASK']))
                        channels.add(channel)
                    if sessions[channel]['CONNECTIONS'].get('GIVE'):
                        sessions[channel]['CONNECTIONS']['GIVE'].sendMessage('SERVICE - CLOSE')
                        log.info('CLOSING: {}'.format(sessions[channel]['CONNECTIONS']['GIVE']))
                        channels.add(channel)
        for channel in channels:
            sessions.pop(channel)
        log.info('ALIVE SESSIONS: {}'.format(str(len(sessions))))


def main(argv=None):
    if argv is None:
        argv = sys.argv[1:]

    parser = OptionParser(u'usage: %prog [options]')

    parser.add_option(
        '--debug',
        action='store_true',
        help=u'Enable debug mode'
    )

    parser.add_option(
        '--info',
        action='store_true',
        help=u'Enable info mode'
    )
    parser.add_option(
        '--config',
        action='store',
        help=u'Config file'
    )

    options, arguments = parser.parse_args(argv)
    logging_setup(options)

    # Get config file
    if options.config:
        config_uri = os.path.abspath(options.config)
    elif os.path.exists('{}/.config/opendesktopserver/settings.ini'.format(os.path.expanduser("~"))):
        config_uri = '{}/.config/opendesktopserver/settings.ini'.format(os.path.expanduser("~"))
    elif os.path.exists('/etc/opendesktopserver/settings.ini'):
        config_uri = '/etc/opendesktopserver/settings.ini'
    else:
        config_uri = os.path.abspath(
            '{}{}..{}config{}settings.ini'.format(os.path.dirname(os.path.realpath(__file__)), os.sep, os.sep, os.sep)
        )
    if not os.path.exists(config_uri):
        log.error('Error, config file not found: {}'.format(config_uri))
        exit(2)
    else:
        log.info('Use config file: {}'.format(config_uri))

    # Extract config parameters
    config = SafeConfigParser()
    config.read(config_uri)
    server = SimpleWebSocketServer(
        config.get('server', 'listening_interface'),
        config.getint('server', 'port'),
        OpenDesktopServer,
        config,
    )
    server.serveforever()


if __name__ == "__main__":
    main(sys.argv)
