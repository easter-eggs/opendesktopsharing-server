#!/usr/bin/env python
# *-* coding: utf-8 *-*

"""
OpenDesktopServer

OpenDesktopServer is a part of OpenDesktopSharing

The OpenDesktopSharing's goal is to access to remote computer across firewall and proxy:
- No needed to know IPs address
- Access only need KEY/PASSWORD
- Use websocket VPN to connect server and client
- Use Auth server (OpenDesktopSharing-Server) to generate auth access

@author: Pierre Arnaud <parnaud@easter-eggs.com>
@copyright: 2017

OpenDesktopClient is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

OpenDesktopClient is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with OpenDesktopClient.  If not, see <http://www.gnu.org/licenses/>.
"""

import os

from setuptools import find_packages
from setuptools import setup


here = os.path.abspath(os.path.dirname(__file__))
README = open(os.path.join(here, 'README.md')).read()
CHANGES = open(os.path.join(here, 'CHANGES.txt')).read()

requires = [
    'websocket-client',
]


setup(
    name='OpenDesktopServer',
    version='0.1',
    description='A tool to share vnc connection over HTTP(s) using WebSockets, this is the server part',
    long_description=README + '\n\n' + CHANGES,
    classifiers=[
        "Programming Language :: Python",
        "Topic :: Internet :: WWW/HTTP",
    ],
    author='Pierre Arnaud',
    author_email='parnaud@easter-eggs.com',
    url='',
    keywords='tcp/ip tunnel websocket vnc',
    packages=find_packages(),
    include_package_data=True,
    zip_safe=True,
    install_requires=requires,
    dependency_links=[
        "git+https://github.com/dpallot/simple-websocket-server.git"
    ],
    entry_points="""\
    [console_scripts]
    opendesktopserver = opendesktopserver.main:main
    """,
)
