# *-* coding: utf-8 *-*

"""
OpenDesktopSharing



The OpenDesktopSharing's goal is to access to remote computer across firewall and proxy:
- No needed to know IPs address
- Access only need KEY/PASSWORD
- Use websocket VPN to connect server and client
- Use Auth server (OpenDesktopSharing-Server) to generate auth access

@author: Pierre Arnaud <parnaud@easter-eggs.com>
@copyright: 2017

OpenDesktopSharing is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

OpenDesktopSharing is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with OpenDesktopSharing.  If not, see <http://www.gnu.org/licenses/>.
"""


import logging
import logging.handlers
import ssl
import sys


BUFFER_SIZE = 2048
MAX_HANDLERS = 255
STATUS_CONNECTED = 'connected'
STATUS_CONNECTING = 'connecting'
STATUS_DISCONNECTED = 'disconnected'
STATUS_ERROR = 'error'
STATUS_QUIT = 'quit'

SSL_VERSIONS = {
    'SSLv23': ssl.PROTOCOL_SSLv23,
    'TLSv1': ssl.PROTOCOL_TLSv1,
    'TLSv1.1': ssl.PROTOCOL_TLSv1_1,
    'TLSv1.2': ssl.PROTOCOL_TLSv1_2,
}


def logging_setup(options):

    l = logging.getLogger()
    if options.debug:
        l.setLevel(logging.DEBUG)
    else:
        l.setLevel(logging.INFO)

    # Syslog handler
    handler = logging.handlers.SysLogHandler('/dev/log')
    handler.setLevel(logging.INFO)

    formatter = logging.Formatter('[%(name)s] %(levelname)-5.5s %(message)s')
    handler.setFormatter(formatter)

    l.addHandler(handler)

    # Console handler (root logger)
    handler = logging.StreamHandler(sys.stderr)
    if options.debug:
        handler.setLevel(logging.DEBUG)
    elif options.info:
        handler.setLevel(logging.INFO)
    else:
        handler.setLevel(logging.ERROR)

    formatter = logging.Formatter('%(asctime)s [%(name)s] %(levelname)-5.5s %(message)s')
    handler.setFormatter(formatter)

    l = logging.getLogger()
    l.addHandler(handler)


def pack(channel, data):
    """ Pack channel and data to a suitable format for message tansport """

    message = bytearray()
    message.append(channel)
    message.extend(data)
    return message


def unpack(message):
    """ Unpack channel and data from message """

    data = bytearray(message)
    channel = data.pop(0)
    return (data, channel)
