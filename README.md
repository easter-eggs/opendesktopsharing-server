OpendesktopServer README
========================
```
apt-get install python-pip
```
Installation
------------
```
sudo ./setup.py develop
```

Dependencies
------------
* symple-websocket-server
* python-websocket

You can install SimpleWebSocketServer by running the following command...
```
sudo pip install git+https://github.com/dpallot/simple-websocket-server.git
```

Use
---
'''
opendesktopserver server.ini --debug
'''